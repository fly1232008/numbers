package com.flyStudio.Numbers;

import android.app.Activity;
import android.app.FragmentManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;
import com.flurry.android.FlurryAgent;
import com.flyStudio.Numbers.data.RecordData;
import com.flyStudio.Numbers.fragments.*;
import com.flyStudio.Numbers.interfaces.*;
import com.flyStudio.Numbers.utils.DbHelper;
import com.flyStudio.Numbers.utils.Dialogs;
import com.flyStudio.Numbers.utils.Game;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements GameController, NavigationListener,GameBroadcastEvent, DatabaseKeeper, GameTimeListener {
    /**
     * Called when the activity is first created.
     */




    /**
     * Сначала структура приложения показалась хорошей
     * по мере увеличения классов, добавления функционала
     * код стал менее интуитивный и масштабируемый, чего старался придерживаться изначально.
     * Комментариев и аннотаций по минимуму, но названия переменных должы сделать код более прозрачным
     */




    String devID = "107047183"; //ID разработчика
    String appID = "207143453"; //ID приложения

    FragmentManager fragmentManager = getFragmentManager();
    ContentFragment content;
    RecordsFragment records;
    MainMenuFragment mainMenu;
    PreferenceFragment preference;
    PreloadFragment preload;
    public static Player player;
    private List<RecordData> data = new ArrayList<RecordData>();
    //GameTimer timer;

    @Override
    public DbHelper getDataBaseHelper() {
        return sqh;
    }

    DbHelper sqh;
    SQLiteDatabase sqdb;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.main);
        FlurryAgent.onStartSession(this, "N8Y56HG66HZQ2QPT25DG");

        sqh = new DbHelper(this);
        // База нам нужна для записи и чтения
        sqdb = sqh.getWritableDatabase();
        player = new Player(this);
        player.setName(sqh.getPlayerName());

        //загружаем список рекордов
        data = sqh.getRecordsList();

        mainMenu = new MainMenuFragment();
        content = new ContentFragment();
        records = new RecordsFragment(data);
        preference = new PreferenceFragment();
        preload = new PreloadFragment();

        fragmentManager.beginTransaction().add(R.id.container, mainMenu).hide(mainMenu).commit();
        fragmentManager.beginTransaction().add(R.id.container, preload).commit();

        Dialogs.init(this);
        Config.init(this);
        globalEvent(Game.START_APP);
    }

    public AdView initAdMob() {
        AdView adView = new AdView(this);
        adView.setAdUnitId(Config.MY_AD_UNIT_ID);
        adView.setAdSize(AdSize.BANNER);
        // Инициирование общего запроса.
        AdRequest adRequest = new AdRequest.Builder().build();
        // Загрузка adView с объявлением.
        adView.loadAd(adRequest);
        return adView;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    long back_pressed = 0;
    @Override
    public void onBackPressed() {
        if (content.getStatus() == Game.START_GAME)
            Dialogs.showDialog(Dialogs.QUITE_GAME_DIALOG);
        else
        if (fragmentManager.popBackStackImmediate()) {
            data = sqh.getRecordsList();
            fragmentManager.popBackStack();
        }
        else {
            if (back_pressed + 2000 > System.currentTimeMillis())
                super.onBackPressed();
            else
                Toast.makeText(getBaseContext(), "Нажмите еще раз чтобы выйти", Toast.LENGTH_SHORT).show();
            back_pressed = System.currentTimeMillis();
        }
    }

    @Override
    public void globalEvent(Game event) {
        switch (event) {
            case NEW_GAME:
                content.onDetach();
                content.onAttach(this);
                fragmentManager.beginTransaction()
                        .remove(content)
                        .add(R.id.container, content)
                        .addToBackStack(null)
                        .commit();
                globalEvent(Game.START_GAME);
                break;
            case GAME_OVER:
                insertToRecord();
                data = sqh.getRecordsList();
                Dialogs.showDialog(Dialogs.GAME_OVER_DIALOG);
                Score.clear();
                break;
            case START_GAME:
                System.out.println("смотрим статус глобальное событие START_GAME " + content.isAdded());
                if (content.isAdded()) {
                    content.setStatus(Game.START_GAME);
                    updateTimer();
                }
                break;
            case START_APP:
                startPreload();
                break;
            default:
                break;
        }
    }

    private void startPreload() {
        content.setStatus(Game.START_APP);
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentManager.beginTransaction()
                .remove(records)
                .remove(content)
                .show(mainMenu)
                .commit();
    }

    @Override
    protected void onPause() {
        System.out.println("хриновые диалоги: onPause");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        System.out.println("хриновые диалоги: onDestroy");
        Dialogs.clear();
        FlurryAgent.onEndSession(this);
        super.onDestroy();
    }



    private void insertToRecord() {
        RecordData recordData = new RecordData();
        recordData.setPlayer_name(player.getName());
        recordData.setPlayer_record(Score.getLevel()+"");
        sqh.insertRecordToDb(recordData);
    }

    @Override
    public void changeNavigationMenu(int type) {
        switch (type) {
            case NEW_GAME:
                if (!content.isVisible()) {
                    fragmentManager.beginTransaction()
                            .remove(mainMenu)
                            .add(R.id.container, content)
                            .addToBackStack(null)
                            .commit();
                }
                break;
            case RECORDS:
                records = new RecordsFragment(data);
                if (!records.isVisible()) {
                    fragmentManager.beginTransaction()
                            .remove(mainMenu)
                            .add(R.id.container, records)
                            .addToBackStack(null)
                            .commit();
                }
                break;
            case PREFERENCE:
                if (!preference.isVisible()) {
                    fragmentManager.beginTransaction()
                            .remove(mainMenu)
                            .add(R.id.container, preference)
                            .addToBackStack(null)
                            .commit();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void toMainMenu() {
        startPreload();
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public void changeName(String newName) {
        try {
            TextView playerName = (TextView) findViewById(R.id.player_name);
            playerName.setText(newName);
        } catch (NullPointerException e) {}
    }

    @Override
    public void changeTimer(int time, long bonus) {
        content.changeTimer(time, bonus);
    }

    @Override
    public void updateTimer() {
        content.updateTimer();
    }

    @Override
    public void pause() {
        content.pause();
    }

    @Override
    public void start() {
        content.start();
    }

    public void onClickTopPanel(View view) {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.flash_anim);
        view.startAnimation(animation);

        switch (view.getId()) {
            case R.id.top_panel_player_settings:
                Dialogs.showDialog(Dialogs.SET_NAME_DIALOG);
                break;
            case R.id.settings:
                changeNavigationMenu(PREFERENCE);
                break;
        }
    }

    public void onClickPreload(View view) {

    }

//======================================================================================================================

}
