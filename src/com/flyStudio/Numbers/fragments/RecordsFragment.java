package com.flyStudio.Numbers.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost;
import com.flyStudio.Numbers.MainActivity;
import com.flyStudio.Numbers.adapters.RecordsAdapter;
import com.flyStudio.Numbers.data.RecordData;
import com.flyStudio.Numbers.interfaces.DatabaseKeeper;
import com.flyStudio.Numbers.utils.DbHelper;
import com.flyStudio.Numbers.R;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fly on 30.06.2014.
 */
public class RecordsFragment extends Fragment {


    private DbHelper sqh;
    List<RecordData> data = new ArrayList<RecordData>();

    public RecordsFragment(List<RecordData> data) {
        this.data = data;
    }

    private AdView adMod;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        adMod = ((MainActivity)activity).initAdMob();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sqh = ((DatabaseKeeper)getActivity()).getDataBaseHelper();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.records, container, false);

        TabHost tabs = (TabHost) view.findViewById(R.id.tabHost);

        tabs.setup();

        TabHost.TabSpec spec = tabs.newTabSpec("local");

        spec.setContent(R.id.local_records);
        spec.setIndicator(getActivity().getResources().getString(R.string.my_records_tab_name));
        tabs.addTab(spec);

        createLocalRecords(view);

        /*spec = tabs.newTabSpec("net");
        spec.setContent(R.id.net_records);
        spec.setIndicator("Рекорды в сети");
        tabs.addTab(spec);*/

        tabs.setCurrentTab(0);

        LinearLayout layout = (LinearLayout)view.findViewById(R.id.ad_mob);
        layout.addView(adMod);

        return view;
    }

    private void createLocalRecords(View view) {
        ListView listView = (ListView) view.findViewById(R.id.local_records_list);
        //List<RecordData> data = sqh.getRecordsList();
        RecordsAdapter adapter = new RecordsAdapter(getActivity(), data);
        listView.setAdapter(adapter);
    }
}