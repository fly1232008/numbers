package com.flyStudio.Numbers.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import com.flyStudio.Numbers.interfaces.NavigationListener;
import com.flyStudio.Numbers.R;

/**
 * Created by Fly on 14.06.2014.
 */
public class NavigationFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.navigation_menuitem_list, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("чтотокакто: i = " + i + " l = " + l);
                ((NavigationListener)getActivity()).changeNavigationMenu(i);
            }
        });
        return view;
    }
}
