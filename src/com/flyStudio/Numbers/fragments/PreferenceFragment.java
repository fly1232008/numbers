package com.flyStudio.Numbers.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.flyStudio.Numbers.Config;
import com.flyStudio.Numbers.MainActivity;
import com.flyStudio.Numbers.utils.Dialogs;
import com.flyStudio.Numbers.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

/**
 * Created by Fly on 07.07.2014.
 */
public class PreferenceFragment extends Fragment {

    private AdView adMod;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        adMod = ((MainActivity)activity).initAdMob();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.preference, container, false);
        Switch autoCake = (Switch) view.findViewById(R.id.settings_auto_cake);
        Switch colorCake = (Switch) view.findViewById(R.id.settings_color_cake);
        RelativeLayout clearRecords = (RelativeLayout) view.findViewById(R.id.settings_clear_records);

        autoCake.setChecked(Config.AUTO_WIDTH_GAME_CAKE);
        colorCake.setChecked(Config.IS_COLOR_GAME_ITEM);

        autoCake.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Config.AUTO_WIDTH_GAME_CAKE = b;
            }
        });
        colorCake.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Config.IS_COLOR_GAME_ITEM = b;
            }
        });

        clearRecords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialogs.showDialog(Dialogs.CLEAR_RECORDS_DIALOG);
            }
        });

        LinearLayout layout = (LinearLayout)view.findViewById(R.id.ad_mob);
        layout.addView(adMod);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}