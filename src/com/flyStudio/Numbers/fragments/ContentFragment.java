package com.flyStudio.Numbers.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.flyStudio.Numbers.Config;
import com.flyStudio.Numbers.MainActivity;
import com.flyStudio.Numbers.Score;
import com.flyStudio.Numbers.adapters.PlaySpaceAdapter;
import com.flyStudio.Numbers.interfaces.*;
import com.flyStudio.Numbers.utils.Dialogs;
import com.flyStudio.Numbers.utils.Game;
import com.flyStudio.Numbers.utils.GameTimer;
import com.flyStudio.Numbers.R;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Fly on 14.06.2014.
 */
public class ContentFragment extends Fragment implements GameSpace, GameTimeListener {
    GridView gridView;
    TextView targetNumber;
    PlaySpaceAdapter adapter;
    View contentView;
    GameTimer timer = null;
    Game status = Game.START_APP;

    int iDisplayHeight = 0;
    int iDisplayWidth = 0;
    int colunpWidth = 0;

    int topPanelHeight = 0;
    private ArrayList<Integer> colorIndex = new ArrayList<Integer>();
    //private ArrayList<Integer> colorIndex = new ArrayList<Integer>();

    public ContentFragment() {

    }

    private AdView adMod;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        System.out.println("Ну и крит! onAttach");
        adMod = ((MainActivity)activity).initAdMob();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        System.out.println("onCreateView");

        View view = inflater.inflate(R.layout.game_space, container, false);
        colunpWidth = getResources().getDimensionPixelOffset(R.dimen.game_space_cake);
        topPanelHeight = getResources().getDimensionPixelOffset(R.dimen.game_space_cake);
        iDisplayHeight = getResources().getDisplayMetrics().heightPixels;
        iDisplayWidth = getResources().getDisplayMetrics().widthPixels;


        gridView = (GridView) view.findViewById(R.id.gridView);

        System.out.println("азмеры: " + topPanelHeight + " а лайаут = " + view.findViewById(R.id.relativeLayout).getHeight());
        //iDisplayHeight = gridView.getDisplay().getHeight();
        List<Integer> data = new ArrayList<Integer>();
        data.add(1);
        adapter = new PlaySpaceAdapter(getActivity(), data, colunpWidth);
        gridView.setAdapter(adapter);

        targetNumber = (TextView) view.findViewById(R.id.target_number);
        targetNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                List<Integer> listData = generateList(Integer.parseInt(charSequence.toString()));
                gridView.setColumnWidth(colunpWidth);
                colunpWidth = getAutoWidth(colunpWidth, gridView.getNumColumns(), listData.size());

                gridView.setColumnWidth(colunpWidth);

                PlaySpaceAdapter playSpace = ((PlaySpaceAdapter)gridView.getAdapter());
                playSpace.setColunpWidth(colunpWidth);
                if (Config.IS_COLOR_GAME_ITEM)
                    playSpace.setColorIndex(colorIndex);
                playSpace.setData(listData);
                playSpace.setNotifyOnChange(true);
                playSpace.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (((GameController)getActivity()).getPlayer() != null)
                    if (!((GameController)getActivity()).getPlayer().getName().equals(""))

                        if (((GameController)getActivity()).getPlayer().move(adapterView.getAdapter().getItem(i).toString())) {
                            int rnd = 1 + (int)(Math.random() * ((Score.MAX_NUM - 1) + 1));
                            targetNumber.setText(rnd+"");
                        }
            }
        });
        contentView = view;
        if (MainActivity.player.getName().equals(""))
            Dialogs.showDialog(Dialogs.SET_NAME_DIALOG);
        else {
            ((GameBroadcastEvent) getActivity()).globalEvent(Game.START_GAME);
        }

        LinearLayout layout = (LinearLayout) view.findViewById(R.id.ad_mob);
        layout.addView(adMod);
        return view;
    }

    private int getAutoWidth(int colunpWidth, int numColumns, int count) {
        if (Config.AUTO_WIDTH_GAME_CAKE)
            if ((iDisplayHeight - (topPanelHeight*3)) < ((count/numColumns) * colunpWidth)) {
            //if ((iDisplayHeight - (topPanelHeight)) < ((count/numColumns) * colunpWidth)) {
                numColumns++;
                colunpWidth = (int)iDisplayWidth / numColumns;
                getAutoWidth(colunpWidth, numColumns, count);
            }
        return colunpWidth;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (timer != null)
            if (timer.isAlive()) {
                timer.start();
            }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (timer != null)
            if (timer.isAlive()) {
                timer.pause();
            }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        setStatus(Game.GAME_OVER);
        if (timer != null) {
            timer.clear();
        }
        Score.clear();
    }

    private List<Integer> generateList(int targetNum) {
        colorIndex = new ArrayList<Integer>();
        int count = Score.getLevel();
        HashMap<Integer, ValuePair> array = new HashMap<Integer, ValuePair>();
        while (array.size() < count+1) {
            double randNum = Math.random();
            int color = (int)(randNum * ((Config.GAME_ITEM_COLORS.length - 2) + 1));
            int rand = 1 + (int)(randNum * ((Score.MAX_NUM - 1) + 1));
            if (rand != targetNum)
                array.put(rand, new ValuePair(rand, color));
        }
        int rnd = 1 + (int)(Math.random() * ((array.size() - 1) + 1));
        array.put(rnd, new ValuePair(targetNum, count));

        List<Integer> result = new ArrayList<Integer>();
        for (ValuePair el : array.values()) {
            result.add(el.getName());
            colorIndex.add(el.getValue());
        }
        return  result;
    }

    public void changeTimerView(int time, long bonus) {
        TextView timerView = (TextView) contentView.findViewById(R.id.game_space_timer);
        if (timerView != null)
            timerView.setText(time+"");
        if (bonus != 0)
            bonusAnimation(bonus);
    }

    private void bonusAnimation(long bonus) {
        System.out.println("аниация: bonusAnimation = " + bonus);
        final TextView bonusView = (TextView)contentView.findViewById(R.id.game_space_bonus);
        bonusView.setText("+" + (int)(bonus/1000));
        Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.bonus_anim);
        bonusView.setVisibility(View.VISIBLE);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                bonusView.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        bonusView.startAnimation(anim);
    }

    @Override
    public void changeTimer(int time, long bonus) {
        changeTimerView(time, bonus);
        changeLevelView();
    }

    private void changeLevelView() {
        TextView levelView = (TextView) contentView.findViewById(R.id.game_space_level);
        if (levelView != null)
            levelView.setText(Score.getLevel()+"");
    }

    @Override
    public void updateTimer() {
        //if (content.STATUS != Game.START_GAME)
        //if (timer == null)
            timer = GameTimer.init(this);
        //else timer.start();
    }

    @Override
    public void pause() {
        if (timer.isAlive())
            timer.pause();
    }

    @Override
    public void start() {
        timer.start();
    }

    @Override
    public void setStatus(Game status) {
        this.status = status;
    }

    @Override
    public Game getStatus() {
        return status;
    }


    public void slideIn(final View view)
    {

    }
}
