package com.flyStudio.Numbers.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.flyStudio.Numbers.MainActivity;
import com.flyStudio.Numbers.adapters.MainMenuAdapter;
import com.flyStudio.Numbers.interfaces.GameController;
import com.flyStudio.Numbers.interfaces.NavigationListener;
import com.flyStudio.Numbers.R;
import com.google.android.gms.ads.AdView;

/**
 * Created by Fly on 30.06.2014.
 */
public class MainMenuFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_menu_fragment, container, false);
        ListView listView = (ListView) view.findViewById(R.id.main_menu);
        String[] array = getResources().getStringArray(R.array.left_navigation);
        MainMenuAdapter adapter = new MainMenuAdapter(getActivity(), array);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("чтотокакто: i = " + i + " l = " + l + " view = " + view.getClass().getSimpleName());
                ((NavigationListener)getActivity()).changeNavigationMenu(i);
            }
        });

        TextView playerName = (TextView) view.findViewById(R.id.player_name);
        playerName.setText(((GameController)getActivity()).getPlayer().getName());
        return view;
    }


}