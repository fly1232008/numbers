package com.flyStudio.Numbers.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.flyStudio.Numbers.interfaces.GameBroadcastEvent;
import com.flyStudio.Numbers.utils.Game;
import com.flyStudio.Numbers.R;

/**
 * Created by Fly on 13.07.2014.
 */
public class PreloadFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.preload, container, false);
        CountDownTimer cc = new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long l) {

            }
            @Override
            public void onFinish() {
                getActivity().getFragmentManager().beginTransaction().remove(PreloadFragment.this).commit();
                ((GameBroadcastEvent)getActivity()).globalEvent(Game.START_APP);
            }
        }.start();
        return view;
    }


}
