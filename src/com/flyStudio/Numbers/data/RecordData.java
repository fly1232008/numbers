package com.flyStudio.Numbers.data;

/**
 * Created by Fly on 30.06.2014.
 */
public class RecordData {
    private int _id;
    private String player_name;
    private String player_record;

    public String getPlayer_name() {
        return player_name;
    }

    public void setPlayer_name(String player_name) {
        this.player_name = player_name;
    }

    public String getPlayer_record() {
        return player_record;
    }

    public void setPlayer_record(String player_record) {
        this.player_record = player_record;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }
}
