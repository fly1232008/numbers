package com.flyStudio.Numbers;

import android.app.Activity;
import android.widget.TextView;
import com.flyStudio.Numbers.interfaces.GameController;

/**
 * Created by Fly on 29.06.2014.
 */
public class Player {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        ((GameController)context).changeName(name);
    }

    private String name;
    Activity context;
    TextView target;
    public Player(Activity activity) {
        context = activity;
    }

    public boolean move(String selectNum) {
        target = (TextView) context.findViewById(R.id.target_number);
        if (selectNum.equals(target.getText().toString())) {
            //верный ход, повышаем уровень
            Score.nextLevel();
            return true;
        }
        else
            return false;
    }
}
