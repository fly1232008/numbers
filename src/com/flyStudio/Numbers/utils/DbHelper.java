package com.flyStudio.Numbers.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import com.flyStudio.Numbers.Config;
import com.flyStudio.Numbers.data.RecordData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fly on 30.06.2014.
 */
public class DbHelper extends SQLiteOpenHelper implements BaseColumns {


    private static final String DATABASE_NAME = "flystudio_numbers.db";
    private static final int DATABASE_VERSION = 1;
    public static final String TABLE_RECORDS = "records";
    public static final String TABLE_PLAYER = "player";
    public static final String PLAYER_NAME = "player_name";
    public static final String PLAYER_RECORD = "player_record";

    private static final String SQL_CREATE_RECORDS = "CREATE TABLE "
            + TABLE_RECORDS + " (" + DbHelper._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + PLAYER_NAME + " VARCHAR(255)," + PLAYER_RECORD + " VARCHAR(255));";
    private static final String SQL_CREATE_PLAYER = "CREATE TABLE "
            + TABLE_PLAYER + " (" + DbHelper._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + PLAYER_NAME + " VARCHAR(255));";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
            + TABLE_RECORDS;

    public DbHelper(Context context) {
        // TODO Auto-generated constructor stub
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(SQL_CREATE_RECORDS);
        db.execSQL(SQL_CREATE_PLAYER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        // Удаляем предыдущую таблицу при апгрейде
        db.execSQL(SQL_DELETE_ENTRIES);
        // Создаём новый экземпляр таблицы
        onCreate(db);
    }

    public List<RecordData> getRecordsList() {
        List<RecordData> result = new ArrayList<RecordData>();
        String query = "SELECT " + DbHelper._ID + ", "
                + DbHelper.PLAYER_NAME + ", " + DbHelper.PLAYER_RECORD + " FROM " + DbHelper.TABLE_RECORDS + " ORDER BY " + PLAYER_RECORD + " DESC " + " LIMIT 0," + Config.RECORDS_LIST_SIZE;

        SQLiteDatabase sqdb = getWritableDatabase();
        Cursor cursor2 = sqdb.rawQuery(query, null);
        while (cursor2.moveToNext()) {
            int id = cursor2.getInt(cursor2.getColumnIndex(DbHelper._ID));
            String name = cursor2.getString(cursor2.getColumnIndex(DbHelper.PLAYER_NAME));
            String record = cursor2.getString(cursor2.getColumnIndex(DbHelper.PLAYER_RECORD));

            System.out.println("PLAYER_RECORD: name = " + name + " record = " + record);

            RecordData recordData = new RecordData();
            recordData.set_id(id);
            recordData.setPlayer_name(name);
            recordData.setPlayer_record(record);
            result.add(recordData);
        }
        cursor2.close();
        return result;
    }

    public void insertRecordToDb(RecordData data) {
        // Метод 1: INSERT через класс CONTENTVALUE
        ContentValues cv = new ContentValues();
        cv.put(DbHelper.PLAYER_NAME, data.getPlayer_name());
        cv.put(DbHelper.PLAYER_RECORD, data.getPlayer_record());
        // вызываем метод вставки
        SQLiteDatabase sqdb = getWritableDatabase();
        sqdb.insert(DbHelper.TABLE_RECORDS, DbHelper._ID, cv);
    }

    public String getPlayerName() {
        String result = new String();
        String query = "SELECT " + DbHelper.PLAYER_NAME + " FROM " + DbHelper.TABLE_PLAYER;

        SQLiteDatabase sqdb = getWritableDatabase();
        Cursor cursor2 = sqdb.rawQuery(query, null);
        cursor2.moveToNext();
        System.out.println("player^ " + cursor2.getCount());
        if (cursor2.getCount() != 0)
            result = cursor2.getString(cursor2.getColumnIndex(DbHelper.PLAYER_NAME));
        else
            return "";
        cursor2.close();
        return result;
    }

    public void setPlayerName(String name) {

        SQLiteDatabase sqdb = getWritableDatabase();

        sqdb.delete(DbHelper.TABLE_PLAYER, null, null);

        ContentValues cv = new ContentValues();
        cv.put(DbHelper.PLAYER_NAME, name);

        sqdb.insert(DbHelper.TABLE_PLAYER, DbHelper._ID, cv);
    }

    public void delitePlayerName() {
        SQLiteDatabase sqdb = getWritableDatabase();
        sqdb.delete(DbHelper.TABLE_PLAYER, null, null);
    }

    public void clearRecordsTable() {
        SQLiteDatabase sqdb = getWritableDatabase();
        sqdb.delete(DbHelper.TABLE_RECORDS, null, null);
    }

}