package com.flyStudio.Numbers.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import com.flyStudio.Numbers.R;
import com.flyStudio.Numbers.interfaces.*;

/**
 * Created by Алексей on 30.06.2014.
 */
public class Dialogs {

    public static final int GAME_OVER_DIALOG = 0;
    public static final int SET_NAME_DIALOG = 1;
    public static final int QUITE_GAME_DIALOG = 2;
    public static final int CLEAR_RECORDS_DIALOG = 3;
    
    private static Dialogs singlton = null;
    private static Activity context = null;
    private static Dialog dialog = null;

    private Dialogs(Activity ctxt) {
        context = ctxt;
        dialog = new Dialog(context);
    }

    public static Dialogs init(Activity ctxt) {
        if (singlton == null) {
            singlton = new Dialogs(ctxt);
            return singlton;
        }
        else return singlton;
    }


    public static void showDialog(int type) {
        switch (type) {
            case GAME_OVER_DIALOG:
                showGameOverDialog();
                break;
            case SET_NAME_DIALOG:
                showSetNameDialog(null);
                break;
            case QUITE_GAME_DIALOG:
                showQuitGameDialog();
                break;
            case CLEAR_RECORDS_DIALOG:
                showClearRecordsDialog();
                break;
        }
    }

    public static void clear() {
        dialog.dismiss();
        dialog = null;
        singlton = null;
    }

    private static void showQuitGameDialog() {
        ((GameTimeListener)context).pause();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        View view = context.getLayoutInflater().inflate(R.layout.exit_game_dialog, null, false);
        ((TextView)view.findViewById(R.id.exit_game_text)).setText(context.getResources().getString(R.string.quit_game_dialog_txt));
        builder.setPositiveButton(context.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((NavigationListener)context).toMainMenu();
            }
        });
        builder.setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((GameTimeListener)context).start();
            }
        });

        builder.setView(view);
        if(!context.isFinishing()) {
            dialog = builder.show();
            WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
            lp.dimAmount=1.0f;
            dialog.getWindow().setAttributes(lp);
        }
    }

    private static void showGameOverDialog() {
        if (dialog.isShowing()) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        View view = context.getLayoutInflater().inflate(R.layout.exit_game_dialog, null, false);
        ((TextView)view.findViewById(R.id.exit_game_text)).setText(context.getResources().getString(R.string.time_over_dialog_txt));
        builder.setPositiveButton(context.getResources().getString(R.string.play_more), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((GameBroadcastEvent)context).globalEvent(Game.NEW_GAME);
            }
        });
        builder.setNegativeButton(context.getResources().getString(R.string.main_menu), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((NavigationListener)context).toMainMenu();
            }
        });

        builder.setView(view);
        if(!context.isFinishing())
            dialog = builder.show();
    }

    private static void showSetNameDialog(String notify) {
        if (dialog.isShowing()) dialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        if (notify == null)
            builder.setTitle(context.getResources().getString(R.string.enter_name_dialog_txt));
        else
            builder.setTitle(notify);
        View view = context.getLayoutInflater().inflate(R.layout.set_name_dialog, null, false);
        final EditText name =(EditText)view.findViewById(R.id.edtSetName);
        name.setText(((GameController)context).getPlayer().getName());
        //name.setText(((DatabaseKeeper)context.getActivity()).getDataBaseHelper().getPlayerName());
        builder.setPositiveButton(context.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //System.out.println("и какое же имя = " + );
                if (!name.getText().toString().equals("")) {
                    ((DatabaseKeeper) context).getDataBaseHelper().setPlayerName(name.getText().toString());
                    ((GameController) context).getPlayer().setName(name.getText().toString());
                    ((GameBroadcastEvent) context).globalEvent(Game.START_GAME);
                } else {showSetNameDialog(context.getResources().getString(R.string.enter_name2_dialog_txt));}
            }
        });
        builder.setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((GameBroadcastEvent) context).globalEvent(Game.START_APP);

            }
        });
        builder.setView(view);
        if(!context.isFinishing())
            dialog = builder.show();
    }

    private static void showClearRecordsDialog() {
        if (dialog.isShowing()) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        View view = context.getLayoutInflater().inflate(R.layout.exit_game_dialog, null, false);
        ((TextView)view.findViewById(R.id.exit_game_text)).setText(context.getResources().getString(R.string.clear_records_list));
        builder.setPositiveButton(context.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((DatabaseKeeper)context).getDataBaseHelper().clearRecordsTable();
            }
        });
        builder.setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setView(view);
        if(!context.isFinishing())
            dialog = builder.show();
    }

}
