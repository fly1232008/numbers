package com.flyStudio.Numbers.utils;

import android.app.Fragment;
import android.os.CountDownTimer;
import com.flyStudio.Numbers.Config;
import com.flyStudio.Numbers.interfaces.GameBroadcastEvent;
import com.flyStudio.Numbers.interfaces.GameSpace;
import com.flyStudio.Numbers.interfaces.GameTimeListener;

import java.util.Calendar;


/**
 * Created by Алексей on 30.06.2014.
 */
public class GameTimer{

    private static TimerTick timer = null;
    private static Fragment context;
    static long bonus = 0;
    public long currentTime;
    public long interval;
    private static GameTimer singlton = null;

    private GameTimer(Fragment ctxt) {
        timer = new TimerTick(Config.START_GAME_TIME, 1000L);
        timer.start();
        context = ctxt;
        lastTime = Calendar.getInstance().getTimeInMillis();
        ((GameSpace)context).setStatus(Game.START_GAME);

    }

    public static GameTimer init(Fragment ctxt) {
        if (singlton == null)
            return singlton = new GameTimer(ctxt);
        else return singlton;
    }

    public void clear() {
        if (timer != null) {
            timer.cancel();
            timer = null;
            singlton = null;
        }
    }

    public void pause() {
        currentTime = timer.currentTime;
        interval = timer.interval;
        timer.cancel();
    }

    public boolean isAlive() {
        if (timer != null)
            if (timer.currentTime == Config.START_GAME_TIME)
                return false;
            else return true;
        else return false;
    }

    public void start() {
        if (timer != null) {
            timer.cancel();
            timer = new TimerTick(currentTime, interval);
            timer.start();
        }
    }

    public static void nextStep(int level) {
        //реализовать добавление бонусов
        bonus = getbonus();
        //запускаем обновление таймера
        updateTimer(level);
    }

    public static void cancelTimer() {
        timer.cancel();
    }

    private static long lastTime;
    private static long getbonus() {
        long curTime = Calendar.getInstance().getTimeInMillis();
        if (curTime - lastTime < Config.MAX_TIME_FOR_BONUS) {
            lastTime = curTime;
            return 1000;
        }
        lastTime = curTime;
        return 0;
    }

    private static void updateTimer(int level) {
        int currentTime = 0;
        if (timer != null) {
            currentTime = (int) (timer.currentTime/1000);
            timer.cancel();
        }
        timer = new TimerTick(generateLevelTime(level, currentTime) + bonus, 1000L);
        timer.start();
    }

    private static long generateLevelTime(int level, int currentTime) {
        return (Config.generateLevelTime(level, currentTime) * 1000);
    }

    //time - общее время, в том числе бонусного = bonus
    public static void changeTimer(int time, long bonus) {
        if (context != null)
            ((GameTimeListener)context).changeTimer(time, bonus);
    }

    private static class TimerTick extends CountDownTimer {

        public long currentTime;
        public long interval;
        public TimerTick(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            currentTime = millisInFuture;
            interval = countDownInterval;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            int time = (int)millisUntilFinished/1000;
            currentTime = millisUntilFinished;
            changeTimer(time, bonus);
            bonus = 0;
        }

        @Override
        public void onFinish() {
            if (context != null) {
                changeTimer(0, 0);
                ((GameBroadcastEvent) context.getActivity()).globalEvent(Game.GAME_OVER);
                ((GameSpace) context).setStatus(Game.GAME_OVER);
            }
            singlton = null;
            timer = null;
        }
    }
}
