package com.flyStudio.Numbers.interfaces;

import com.flyStudio.Numbers.utils.DbHelper;

/**
 * Created by Fly on 30.06.2014.
 */
public interface DatabaseKeeper {
    DbHelper getDataBaseHelper();
}
