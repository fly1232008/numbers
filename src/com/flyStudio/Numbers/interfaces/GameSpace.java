package com.flyStudio.Numbers.interfaces;

import com.flyStudio.Numbers.utils.Game;

/**
 * Created by Алексей on 02.07.2014.
 */
public interface GameSpace {
    public Game STATUS = Game.START_APP;
    public void setStatus(Game status);
    public Game getStatus();
}
