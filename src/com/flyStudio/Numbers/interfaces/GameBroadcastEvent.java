package com.flyStudio.Numbers.interfaces;

import com.flyStudio.Numbers.utils.Game;

/**
 * Created by Алексей on 30.06.2014.
 */
public interface GameBroadcastEvent {
    void globalEvent(Game event);
}
