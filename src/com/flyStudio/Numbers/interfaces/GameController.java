package com.flyStudio.Numbers.interfaces;

import com.flyStudio.Numbers.Player;

/**
 * Created by Fly on 29.06.2014.
 */
public interface GameController {
    public Player getPlayer();
    public void changeName(String newName);
}
