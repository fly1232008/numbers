package com.flyStudio.Numbers.interfaces;

/**
 * Created by Fly on 07.07.2014.
 */
public class ValuePair {
    public ValuePair(int name, int value) {
        this.name = name;
        this.value = value;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    int name;
    int value;
}
