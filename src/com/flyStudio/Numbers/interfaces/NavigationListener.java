package com.flyStudio.Numbers.interfaces;

/**
 * Created by Fly on 29.06.2014.
 */
public interface NavigationListener {
    static final int NEW_GAME = 0;
    static final int RECORDS = 1;

    static final int PREFERENCE = 99;

    void changeNavigationMenu(int type);
    void toMainMenu();
}
