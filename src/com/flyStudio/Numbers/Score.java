package com.flyStudio.Numbers;

import com.flyStudio.Numbers.utils.GameTimer;

/**
 * Created by Fly on 29.06.2014.
 */
public class Score {
    public static int MAX_NUM = 3;

    public static int getLevel() {
        return level;
    }

    static int level = 1;
    public static void nextLevel() {
        //повышаем уровень и обновляем таймер
        level++;
        MAX_NUM = level * 3;
        GameTimer.nextStep(level);
    }

    public static void clear() {
        level = 1;
        MAX_NUM = 3;
    }
}
