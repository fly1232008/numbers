package com.flyStudio.Numbers.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.flyStudio.Numbers.Config;
import com.flyStudio.Numbers.Score;
import com.flyStudio.Numbers.custom_view.Cake;
import com.flyStudio.Numbers.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fly on 29.06.2014.
 */
public class PlaySpaceAdapter extends ArrayAdapter<Integer> {
    private ArrayList<Integer> colorIndex = new ArrayList<Integer>();

    public void setData(List<Integer> data) {
        this.data = data;
    }

    private List<Integer> data;
    private Activity context;
    String[] colors;


    public void setColunpWidth(int colunpWidth) {
        this.colunpWidth = colunpWidth;
    }

    int colunpWidth;

    public PlaySpaceAdapter(Activity context, List<Integer> data, int colunpWidth) {
        super(context, R.layout.play_space_item);
        this.context = context;
        this.data = data;
        this.colunpWidth = colunpWidth;
        System.out.println("почеу: size = " + data.size());
        for (Integer el : data) {
            System.out.println("почеу: элементы = " + el);
        }
        colors = Config.GAME_ITEM_COLORS;
    }

    @Override
    public int getCount() {
        // возвращаем количество элементов списка
        return data.size();
    }

    @Override
    public Integer getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            view = context.getLayoutInflater().inflate(R.layout.play_space_item, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.play_space_num);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Cake cake = (Cake)((ViewGroup)view).getChildAt(0);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(colunpWidth, colunpWidth);
        cake.setLayoutParams(params);
        cake.setPadding(1,1,1,1);
        if (Score.MAX_NUM <= 999)
            holder.name.setTextSize((float) (colunpWidth / 4.1));
        else
            holder.name.setTextSize((float) (colunpWidth / 5.5));

        Integer element = data.get(position);
        holder.name.setText(element+"");

        //int colorIntex = 1 + (int)(Math.random() * ((Config.GAME_ITEM_COLORS.length - 2) + 1));
        if (colorIndex.size() != 0) {
            int index = colorIndex.get(position);
            if (colors.length > index) {
                holder.name.setTextColor(Color.parseColor("#" + colors[index]));
            }
        }

        return view;
    }

    public void setColorIndex(ArrayList<Integer> colorIndex) {
        this.colorIndex = colorIndex;
    }

    private class ViewHolder {
        public TextView name;
    }
}
