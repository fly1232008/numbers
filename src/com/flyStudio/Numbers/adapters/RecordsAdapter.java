package com.flyStudio.Numbers.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.flyStudio.Numbers.data.RecordData;
import com.flyStudio.Numbers.R;

import java.util.List;

/**
 * Created by Fly on 30.06.2014.
 */
public class RecordsAdapter extends ArrayAdapter<RecordData> {
    private List<RecordData> data;
    private Activity context;

    public RecordsAdapter(Activity context, List<RecordData> data) {
        super(context, R.layout.records_list_item);
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        // возвращаем количество элементов списка
        return data.size();
    }

    @Override
    public RecordData getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            view = context.getLayoutInflater().inflate(R.layout.records_list_item, parent, false);
            holder = new ViewHolder();
            holder.text1 = (TextView) view.findViewById(R.id.records_item_text1);
            holder.text2 = (TextView) view.findViewById(R.id.records_item_text2);
            holder.text3 = (TextView) view.findViewById(R.id.records_item_text3);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        RecordData element = data.get(position);
        holder.text1.setText(position+1+"");
        holder.text2.setText(element.getPlayer_name());
        holder.text3.setText(element.getPlayer_record());

        return view;
    }

    private class ViewHolder {
        public TextView text1;
        public TextView text2;
        public TextView text3;
    }
}
