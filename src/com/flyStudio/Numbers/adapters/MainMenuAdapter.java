package com.flyStudio.Numbers.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.flyStudio.Numbers.R;

/**
 * Created by Fly on 05.07.2014.
 */
public class MainMenuAdapter extends ArrayAdapter<String> {
    private String[] data;
    private Activity context;

    public MainMenuAdapter(Activity context, String[] data) {
        super(context, R.layout.main_menu_item);
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        // возвращаем количество элементов списка
        return data.length;
    }

    @Override
    public String getItem(int position) {
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            view = context.getLayoutInflater().inflate(R.layout.main_menu_item, parent, false);
            holder = new ViewHolder();
            holder.text = (TextView) view.findViewById(R.id.main_menu_text);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String element = data[position];
        holder.text.setText(element);

        return view;
    }

    private class ViewHolder {
        public TextView text;
    }
}
