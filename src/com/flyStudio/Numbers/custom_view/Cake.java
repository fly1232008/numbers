package com.flyStudio.Numbers.custom_view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by Алексей on 04.07.2014.
 */
public class Cake extends LinearLayout {
    Context context;

    public Cake(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public Cake(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public Cake(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    private void init() {
    }
}
