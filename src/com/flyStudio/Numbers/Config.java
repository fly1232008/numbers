package com.flyStudio.Numbers;

import android.content.Context;

/**
 * Created by Fly on 30.06.2014.
 */
public class Config {

//======================================================================================================================
    public static final String MY_AD_UNIT_ID = "ca-app-pub-4667278565281772/5021297998";
//======================================================================================================================

    public static final long MAX_TIME_FOR_BONUS = 3000;

    public static long START_GAME_TIME = 5000L;
    public static boolean AUTO_WIDTH_GAME_CAKE = true;
    public static int RECORDS_LIST_SIZE = 15;
    public static String[] GAME_ITEM_COLORS;
    public static boolean IS_COLOR_GAME_ITEM = true;

    static Context context;
    static Config singlton = null;
    private Config(Context ctxt) {
        this.context = ctxt;
        GAME_ITEM_COLORS = context.getResources().getStringArray(R.array.game_item_colors);
        for (String el : GAME_ITEM_COLORS) {
            System.out.println("массив цветов = col = " + el);
        }

    }



    //return secounds
    //y = кореь из x
    public static int generateLevelTime(int level, int currentSecounds) {
        int result = 0;
        double sqrt = Math.sqrt(level);
        double index = level / 10;
        if (sqrt > index) {
            result = (int) (sqrt - index + currentSecounds + 1);
        } else
            result = (int) (sqrt - (index / 2) + currentSecounds + 1);
        return result;
    }

    public static Config init(Context context) {
        loadConfig();
        if (singlton == null)
            singlton = new Config(context);
        return singlton;
    }

    private static void loadConfig() {

    }
}
